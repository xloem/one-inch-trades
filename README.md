### Simple set of functions to get you started making trades on 1 Inch DEX using Web3 and Python

A basic walk though of using the scripts can be found [here](https://wolfdefi.com/posts/2020/04/1-inch-python-web3-trading-scripts/). 

### Examples of functions you can use in the script 
Here are the steps for ETH to DAI exchange using 1 inch split contract (no apis)

`one_inch_token_swap(ethereum, mcd_contract_address, amount_to_exchange)`

Here are the steps for a DAI to ETH exchange using 1 inch split contract (no apis) this is more complex because we need to approve 1 Inch contract to spend our DAI first

1. approve our DAI transfer

   `approve_ERC20(amount_of_dai)`

2. make trade/exchange

   `one_inch_token_swap(mcd_contract_address, ethereum, amount_of_dai)`

#--- Here are some examples of single methods/functions being executed ---#

Get a trade quote directly from the blockchain
response is a list like: `[1533867641279495750, [0, 95, 5, 0, 0, 0, 0, 0, 0, 0]]`

Where first item is amount, second is a list of how your order will be distributed across exchanges

`logger.info(one_inch_get_quote(ethereum, mcd_contract_address, amount_to_exchange))`

#--- Making an Approval ---#

Check if MCD contract has allowance for provided account to spend tokens:

`get_allowance(base_account)`

This will approve the one inch split contract to spend to spend amount_of_dai worth of base_account's tokens you will need to call this before trading your MCD/DAI on 1 inch. This will only probably only cost a small bit of gas.

`approve_ERC20(amount_of_dai)`

Check MCD again to confirm approval worked:

`get_allowance(base_account)`

#--- Using API to get data and make trades ---#

`get_api_quote_data("DAI", "ETH", amount_to_exchange)`

`get_api_call_data("DAI", "ETH", amount_to_exchange)`
